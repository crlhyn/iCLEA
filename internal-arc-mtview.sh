node=$(ssh -XY discover.nccs.nasa.gov hostname)  #e.g. discover 62

#x11
ssh -XYqt -p2255  $node salloc -N 8 --ntasks-per-node=36 -t 0:33:00 --qos=debug    #8nodes  36 cpus   33mins walltime

#or not X11
# salloc -N 2 --ntasks-per-node=16 -t 0:30:00 --qos=debug --constraint=sp1


#others https://modelingguru.nasa.gov/docs/DOC-1869
