#!/usr/bin/bash #SBATCH -J Test_Slurm_Job
#SBATCH --ntasks=1 --cpus-per-task=6 --constraint=hasw
#SBATCH --time=1:00:00
#SBATCH -o output.%j
#SBATCH --account=xxxx
#SBATCH --workdir=/discover/nobackup/myuserid
export OMP_NUM_THREADS=6
# the line above is optional if "--cpus-per-task=" is set
export OMP_STACKSIZE=1G
export KMP_AFFINITY=scatter
./Test_omp_executable
exit 0
